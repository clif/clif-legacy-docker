docker-clif
===========

![CLIF logo](https://raw.githubusercontent.com/dillense/docker-clif/master/clif_100.png "CLIF is a Load Injection Framework")

what to do with it?
-------------------

[CLIF is an open source performance testing software.](http://clif.ow2.org)

Practically, docker-clif is based on a Ubuntu 24.04 image, with Java OpenJDK headless JDK version 17, and CLIF-2.3.9-server CLIF distribution package.

The provided CLIF runtime is suitable for using all line commands of CLIF. Therefore, this image allows for:
- running a CLIF registry (for registering CLIF servers),
- running a CLIF server (for deploying load injectors and probes),
- deploying and running CLIF test plans over CLIF servers,
- getting a quick statistical overview of response times for each test run (not mentioning the complete set of detailed measurements available in the usual `report` folder).

Notes:
- for writing test scenarios, consider using [CLIF's main GUI based on Eclipse](https://release.ow2.org/clif/2.3.9/), or the [CLIF Web user interface](https://gitlab.ow2.org/clif/clif-webui), supporting edition of scenarios using both XML and YAML formats, and coming with very useful wizards;
- for automatic performance test runs and reports, think about using [CLIF's plug-in](https://plugins.jenkins.io/clif-performance-testing/) for [Jenkins](https://jenkins.io/). Consider using Docker image `dillense/jenkins-clif`.

usage
-----

- `$ docker pull dillense/clif`

    Import the docker-clif image to your local repository.

- `$ docker images`

    Check the docker-clif image is in your local repository.

- `$ docker run -ti dillense/clif`

    Launch a docker-clif container with an interactive shell, logged as user clif and with current directory set to /home/clif. All CLIF commands are available right away through `clifcmd`.

- `$ docker run -ti --network host clif`

    Same as above, except current host address is used instead of Docker's default bridge NAT'ed address. Option "--network host" is mandatory as soon as you perform distributed testing with CLIF servers (docker-clif instances) spread over different containers, because CLIF's networking internals don't support NAT.

- `$ docker run --network host clif -e COMMAND="registry" -e REGISTRY=<myhost-ip-address>`

    Runs a CLIF registry that will listen to the host address _\<myhost-ip-address>_ (it must be an existing address of the host on which you run the command).

- `$ docker run --network host clif -e COMMAND="server clif1" -e REGISTRY=<registry-ip-address> -e CODESERVER=<codeserver-ip-address>`

    Runs a CLIF server named _clif1_ that will register at the CLIF registry at the given _\<registry-ip-address>_, and that will use the CLIF code server at the given _\<codeserver-ip-address>_ to get all necessary resources when a probe or injector is being deployed (scenario files, or any file of any type of content used by the load injector or probe).  
    __Note.__ The code server address is the address of the host from which you will deploy CLIF test plans.

Refer to chapter 8.2 of [CLIF's user manual](https://clif.ow2.io/doc/user_manual/manual/UserManual.pdf) for a complete reference of clif commands. Test plans and scenarios examples may be found [here](https://clif.ow2.io/clif-legacy/download/clif-examples.zip "Download CLIF examples"). Some CLIF command examples are given below.

example
-------
```
$ docker run --rm -ti dillense/clif
clif@845774c7c9da:~$ wget https://clif.ow2.io/clif-legacy/download/clif-examples.zip
[...]
clif@845774c7c9da:~$ unzip clif-examples.zip
[...]
clif@845774c7c9da:~$ cd examples
clif@845774c7c9da:~/examples$ clifcmd launch dummy dummy.ctp dummy1
Trying to connect to an existing CLIF Registry...
Failed.
Creating a CLIF Registry...
Fractal registry is ready.
Deploying from dummy.ctp test plan definition...
Test plan dummy is deployed.
Initializing
Initialized
Starting dummy test plan...
Started
Waiting for test termination...
Collecting data from test plan dummy...
Collection done
test run dummy complete
clif@845774c7c9da:~/examples$ clifcmd quickstats
action type	calls	min	max	mean	median	std dev	throughput	errors
dummy action 	   536	     1	   998	504.703	   503	288.320	17.974	    63
GLOBAL LOAD	   536	     1	   998	504.703	   503	288.320	17.974	    63
Quick statistics report done
clif@845774c7c9da:~/examples$ more report/dummy1*/0/action
# date, session id, action type, iteration, success, duration, comment, result
261,0,dummy action,0,true,378,successful dummy request,378
261,17,dummy action,0,true,638,successful dummy request,638
261,19,dummy action,0,true,215,successful dummy request,215
[...]
```
