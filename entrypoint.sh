#!/bin/sh -l
cd /home/clif

clifcmd config $REGISTRY $CODESERVER

if test -n "$COMMAND"
then
    exec clifcmd $COMMAND
else
    exec /bin/bash -l
fi